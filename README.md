Install cypress:

npm i -D cypress

Install the package:

npm i -D cypress-image-diff-js

Run single test

npx cypress run  --spec "cypress/integration/Consulting/01.NewYork.spec.js"

npx cypress run --browser chrome --spec "cypress/integration/Consulting/01.NewYork.spec.js"

npx cypress run --browser firefox --spec "cypress/integration/Consulting/01.NewYork.spec.js"

Run Folder Tests

npx cypress run  --spec "cypress/integration/Consulting/*"    - CONSULTING

npx cypress run  --spec "cypress/integration/Masterstudy/*"   - MASTERSTUDY

npx cypress run  --spec "cypress/integration/Motors/*"        - MOTORS

npx cypress run  --spec "cypress/integration/Pearl/*"         - PEARL

npx cypress run  --spec "cypress/integration/Splash/*"        - Splash 
