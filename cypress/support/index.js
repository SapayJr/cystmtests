import './commands'
require('./commands')
Cypress.Cookies.defaults({
  preserve: "csrftoken"
})

after(() => {
    cy.task('generateReport')
  })
