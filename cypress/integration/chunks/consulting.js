export default function ConsultingRegressionTesting(url,demo) {



    let demos = [

        ,"https://consulting.stylemixthemes.com/los-angeles/"
        ,"https://consulting.stylemixthemes.com/five/"
        , "https://consulting.stylemixthemes.com/jakarta/"
        , "https://consulting.stylemixthemes.com/miami/"
        , "https://consulting.stylemixthemes.com/geneva/"
        ,"https://consulting.stylemixthemes.com/zurich/"
        ,"https://consulting.stylemixthemes.com/marseille/"
        ,"https://consulting.stylemixthemes.com/mumbai/"
        ,"https://consulting.stylemixthemes.com/new-delhi/"
        ,"https://consulting.stylemixthemes.com/lisbon/"
        ,"https://consulting.stylemixthemes.com/melbourne/"
        ,"https://consulting.stylemixthemes.com/thirteen/"
        ,"https://consulting.stylemixthemes.com/stockholm/"
        ,"https://consulting.stylemixthemes.com/two/"
        ,"https://consulting.stylemixthemes.com/berlin/"
        ,"https://consulting.stylemixthemes.com/eight/"
        ,"https://consulting.stylemixthemes.com/twelve/"
        ,"https://consulting.stylemixthemes.com/liverpool/"
        ,"https://consulting.stylemixthemes.com/budapest/"
        , "https://consulting.stylemixthemes.com/barcelona/"
        ,"https://consulting.stylemixthemes.com/brussels/"
        ,"https://consulting.stylemixthemes.com/milan/"
        ,"https://consulting.stylemixthemes.com/lyon/"
        , "https://consulting.stylemixthemes.com/osaka"
        , "https://consulting.stylemixthemes.com/ankara"
        ,"https://consulting.stylemixthemes.com/nineteen/"
        ,"https://consulting.stylemixthemes.com/three/"
        , "https://consulting.stylemixthemes.com/toronto/"
        ,"https://consulting.stylemixthemes.com/beijing/"
        , "https://consulting.stylemixthemes.com/san-francisco/"
        , "https://consulting.stylemixthemes.com/six/"
        , "https://consulting.stylemixthemes.com/seven/"
        ,"https://consulting.stylemixthemes.com/fourteen/"
        ,"https://consulting.stylemixthemes.com/istanbul/"
        , "https://consulting.stylemixthemes.com/nine/"
        ,"https://consulting.stylemixthemes.com/vienna/"
        ,"https://consulting.stylemixthemes.com/four/"
        ,"https://consulting.stylemixthemes.com/fifteen/"
        ,"https://consulting.stylemixthemes.com/denver/"
        ,"https://consulting.stylemixthemes.com/amsterdam/"
        , "https://consulting.stylemixthemes.com/eleven/"
        , "https://consulting.stylemixthemes.com/davos/"
        , "https://consulting.stylemixthemes.com/ten/"
        ,"https://consulting.stylemixthemes.com/sixteen/"
        , "https://consulting.stylemixthemes.com/twenty/"
        ,"https://consulting.stylemixthemes.com/seventeen/"
        ,"https://consulting.stylemixthemes.com/eighteen/"
        ,'http://demo-import.stylemixstage.com/'
    ]

    let uniqueLinks = [], domLinks = [], isSameURl = false;
    beforeEach(() => {
        cy.viewport(1920, 1080)
      })
      Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })

    it('Visual Regression Testing', () => {

        cy.visit(url);
        cy.document().then((doc) => {
            var x = doc.querySelectorAll('a');
                for (var i=0; i<x.length; i++){
                    if( x[i].href.includes(x[i].baseURI) &&
                         !x[i].href.includes(x[i].baseURI+"#" &&
                            !x[i].href.includes(x[i].baseURI+"landing/")
                         )) {

                        console.log('site link = ',x[i].href)
                        let siteLink = x[i].href.toString();
                        if(siteLink.indexOf('/#')> -1 || siteLink.indexOf('/?')> -1 || siteLink.indexOf('/landing')> -1){
                            console.log('reshetka = ',x[i].href)
                        }
                        else {
                            var nametext = x[i].textContent;
                            var cleantext = nametext.replace(/\s+/g, ' ').trim();
                            var cleanlink = x[i].href;
                            domLinks.push([cleantext,cleanlink]);
                        }
                    }
                };
            for(var i=0; i<domLinks.length; i++){
                for(var j=0; j<uniqueLinks.length; j++){
                    if(domLinks[i][1] == uniqueLinks[j][1]){
                        isSameURl = true;
                    }
                }
                if(!isSameURl){
                    uniqueLinks.push(domLinks[i][1]);
                }
                else {
                    isSameURl = false;
                }
            }

            var arrayWithoutDuplicates = [];
            for(i=0; i < uniqueLinks.length; i++){
                if(arrayWithoutDuplicates.indexOf(uniqueLinks[i]) === -1) {
                    arrayWithoutDuplicates.push(uniqueLinks[i]);
                }
            }

            if(demo === 'NewYork','Dublin') {
                arrayWithoutDuplicates = arrayWithoutDuplicates.filter(function(x) {
                    return demos.indexOf(x) < 0;
                });
            }

            cy.log('Links: ', arrayWithoutDuplicates.length)

            for(var i=0; i<arrayWithoutDuplicates.length; i++) {

                let siteLink = arrayWithoutDuplicates[i];

                cy.log('left: ', (arrayWithoutDuplicates.length-i))
                cy.task('log', '  Visiting  - ' + arrayWithoutDuplicates[i])

                cy.task('log', '  Visited  - ' + arrayWithoutDuplicates[i])
                cy.wait(4000);

                let siteUrlForLog = arrayWithoutDuplicates[i];
                cy.visit({url:siteLink, failOnStatusCode: false});

                cy.document().then((dom) => {

                    dom.body.className = dom.body.className.replace("sticky_menu","");
                    dom.body.className = dom.body.className.replace("affix","");

                    const removeElements = (elms) => elms.forEach(el => el.remove());
                    removeElements( dom.querySelectorAll(".pearl-envato-preview") );
                    removeElements( dom.querySelectorAll(".pearl-envato-preview-holder") );
                    removeElements( dom.querySelectorAll(".stm_theme_demos") );

                    // dom.getElementsByClassName('header_top clearfix affix').setAttribute("style", "top:0px !important");
                    if(dom.documentElement.offsetHeight > dom.documentElement.scrollHeight ){
                        cy.task('log', '  Scrolling  - ' + siteUrlForLog)

                        cy.scrollTo('0%', '25%',{ duration: 2000 })
                        cy.scrollTo('0%', '50%',{ duration: 2000 })
                        cy.scrollTo('0%', '75%',{ duration: 2000 })
                        cy.scrollTo('0%', '100%',{ duration: 2000 })

                        cy.wait(2000)
                        cy.task('log', '  Scrolling Ended  - ' + siteUrlForLog)

                    }
                });

                var screenshotName = arrayWithoutDuplicates[i].replace('https://','').replaceAll('/','__');
                screenshotName = screenshotName.replaceAll('.','__').toString();

                cy.wait(4000);
                cy.task('log', '  Executing a script for a screenshot  - ' + siteUrlForLog)

                cy.compareSnapshot(screenshotName, 0.3);

                cy.task('log', '  Page Index  - ' + i)
                cy.task('log', '  Screenshot successfully taken from this page - ' + arrayWithoutDuplicates[i])
                cy.task('log', '  Number of pages remaining: ' + (arrayWithoutDuplicates.length-i))
                cy.task('log', '   ')

            }

        })

    })
}



