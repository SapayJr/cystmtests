export default function ConsultingRegressionTestingAllDemos(url) {



    let demos = [

        ,"https://consulting.stylemixthemes.com/los-angeles/"
        ,"https://consulting.stylemixthemes.com/five/"
        , "https://consulting.stylemixthemes.com/jakarta/"
        , "https://consulting.stylemixthemes.com/miami/"
        , "https://consulting.stylemixthemes.com/geneva/"
        ,"https://consulting.stylemixthemes.com/zurich/"
        ,"https://consulting.stylemixthemes.com/marseille/"
        ,"https://consulting.stylemixthemes.com/mumbai/"
        ,"https://consulting.stylemixthemes.com/new-delhi/"
        ,"https://consulting.stylemixthemes.com/lisbon/"
        ,"https://consulting.stylemixthemes.com/melbourne/"
        ,"https://consulting.stylemixthemes.com/thirteen/"
        ,"https://consulting.stylemixthemes.com/stockholm/"
        ,"https://consulting.stylemixthemes.com/two/"
        ,"https://consulting.stylemixthemes.com/berlin/"
        ,"https://consulting.stylemixthemes.com/eight/"
        ,"https://consulting.stylemixthemes.com/twelve/"
        ,"https://consulting.stylemixthemes.com/liverpool/"
        ,"https://consulting.stylemixthemes.com/budapest/"
        , "https://consulting.stylemixthemes.com/barcelona/"
        ,"https://consulting.stylemixthemes.com/brussels/"
        ,"https://consulting.stylemixthemes.com/milan/"
        ,"https://consulting.stylemixthemes.com/lyon/"
        , "https://consulting.stylemixthemes.com/osaka"
        , "https://consulting.stylemixthemes.com/ankara"
        ,"https://consulting.stylemixthemes.com/nineteen/"
        ,"https://consulting.stylemixthemes.com/three/"
        , "https://consulting.stylemixthemes.com/toronto/"
        ,"https://consulting.stylemixthemes.com/beijing/"
        , "https://consulting.stylemixthemes.com/san-francisco/"
        , "https://consulting.stylemixthemes.com/six/"
        , "https://consulting.stylemixthemes.com/seven/"
        ,"https://consulting.stylemixthemes.com/fourteen/"
        ,"https://consulting.stylemixthemes.com/istanbul/"
        , "https://consulting.stylemixthemes.com/nine/"
        ,"https://consulting.stylemixthemes.com/vienna/"
        ,"https://consulting.stylemixthemes.com/four/"
        ,"https://consulting.stylemixthemes.com/fifteen/"
        ,"https://consulting.stylemixthemes.com/denver/"
        ,"https://consulting.stylemixthemes.com/amsterdam/"
        , "https://consulting.stylemixthemes.com/eleven/"
        , "https://consulting.stylemixthemes.com/davos/"
        , "https://consulting.stylemixthemes.com/ten/"
        ,"https://consulting.stylemixthemes.com/sixteen/"
        , "https://consulting.stylemixthemes.com/twenty/"
        ,"https://consulting.stylemixthemes.com/seventeen/"
        ,"https://consulting.stylemixthemes.com/eighteen/"
    ]

    let uniqueLinks = [], domLinks = [], isSameURl = false;

    describe('Visual Regression Testing', () => {

        var arrayWithoutDuplicates = [];
        it(url, function () {
            cy.visit(url);
            cy.document().then((doc) => {

            var x = doc.querySelectorAll('a');
            for (var i=0; i<x.length; i++){
                if(
                    x[i].href.includes(x[i].baseURI) &&
                    !x[i].href.includes(x[i].baseURI+"#" &&
                        !x[i].href.includes(x[i].baseURI+"landing/"
                        ))){

                    console.log('site link = ',x[i].href)
                    let siteLink = x[i].href.toString();
                    if(siteLink.indexOf('/#')> -1 || siteLink.indexOf('/?')> -1 || siteLink.indexOf('/landing')> -1){
                        console.log('reshetka = ',x[i].href)

                    }
                    else {
                        var nametext = x[i].textContent;
                        var cleantext = nametext.replace(/\s+/g, ' ').trim();
                        var cleanlink = x[i].href;
                        domLinks.push([cleantext,cleanlink]);
                    }
                }
            };
            for(var i=0; i<domLinks.length; i++){
                for(var j=0; j<uniqueLinks.length; j++){
                    if(domLinks[i][1] == uniqueLinks[j][1]){
                        isSameURl = true;
                    }
                }
                if(!isSameURl){
                    uniqueLinks.push(domLinks[i][1]);
                }
                else {
                    isSameURl = false;
                }
            }


            for(i=0; i < uniqueLinks.length; i++){
                if(arrayWithoutDuplicates.indexOf(uniqueLinks[i]) === -1) {
                    arrayWithoutDuplicates.push(uniqueLinks[i]);
                }
            }

            cy.log('Links: ', arrayWithoutDuplicates.length)

            if(url === 'https://consulting.stylemixthemes.com/'){
                arrayWithoutDuplicates = arrayWithoutDuplicates.filter(function(x) {
                    return demos.indexOf(x) < 0;
                });
                cy.log('filtered demos')
            }
            });
        });

        it('Visit and screen capture', function () {

            cy.document().then((doc) => {

                for (var i = 0; i < arrayWithoutDuplicates.length; i++) {
                    cy.log('left: ', (arrayWithoutDuplicates.length - i))
                    let siteLink = arrayWithoutDuplicates[i];
                    cy.task('log', 'Visiting  - ' + url)



                        cy.visit({url: siteLink, failOnStatusCode: false});
                        cy.get('body').its('.pearl-envato-preview').then(res => {

                            if (res > 0) {
                                cy.get('.pearl-envato-preview').invoke('attr', 'style', 'display: none');
                                cy.get('.pearl-envato-preview-holder').invoke('attr', 'style', 'display: none');
                                cy.get('.preview__envato-logo').invoke('attr', 'style', 'display: none');
                                cy.get('.preview__actions').invoke('attr', 'style', 'display: none');
                            } else {
                                console.log('iframe false')
                            }



                        cy.document().then((dom) => {
                            dom.body.className = dom.body.className.replace("sticky_menu", "");
                            dom.body.className = dom.body.className.replace("affix", "");
                            const removeElements = (elms) => elms.forEach(el => el.remove());
                            removeElements(dom.querySelectorAll(".pearl-envato-preview"));
                            removeElements(dom.querySelectorAll(".pearl-envato-preview-holder"));

                            // dom.getElementsByClassName('header_top clearfix affix').setAttribute("style", "top:0px !important");
                            cy.task('log', 'Scrolling  - ' + url)

                            if (dom.documentElement.scrollHeight > 1080) {
                                cy.scrollTo('0%', '25%', {duration: 2000})
                                cy.scrollTo('0%', '50%', {duration: 2000})
                                cy.scrollTo('0%', '75%', {duration: 2000})
                                cy.scrollTo('0%', '100%', {duration: 2000})
                                cy.wait(2000)
                            }


                        var screenshotName = arrayWithoutDuplicates[i].replace('https:', '').replaceAll('-', '__').toString();
                        // screenshotName = screenshotName.replaceAll('.','__').toString();

                        var str = screenshotName;
                        var count = 0;
                        var urlForAll = "";
                        var isEnough = false;

                        for (var j = 0; j < str.length; j++) {

                            if (str[j] == '/') {

                                count++;

                                if (isEnough) {
                                    urlForAll = urlForAll + '__';
                                } else {
                                    urlForAll = urlForAll + str[j];
                                }
                            } else {
                                urlForAll = urlForAll + str[j];
                            }


                            if (count == 4) {
                                isEnough = true;
                            }

                        }


                        cy.wait(4000);
                        if (urlForAll[urlForAll.length - 1] == '/') {
                            urlForAll = urlForAll + '__';
                        }
                        cy.task('log', 'Executing a script for a screenshot  - ' + url)
                        cy.matchImageSnapshot(urlForAll);
                        cy.task('log', 'Page Index  - ' + i)
                        cy.task('log', 'Screenshot successfully taken from this page - ' + arrayWithoutDuplicates[i])
                        cy.task('Number of pages remaining: ', (arrayWithoutDuplicates.length - i))

                    });
                        });
                }

            })
        });

    })
}



