describe('Create 3 Calculator Fields', ()=>{

    beforeEach(() => {
        cy.viewport(1920, 1080)
      })

    it('Add three checkboxes with optional values', () =>{       
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#toplevel_page_cost_calculator_builder > .wp-has-submenu > .wp-menu-name').click()
        cy.wait(2000)
        cy.get('.button-wrapper > button:nth-child(2)').click()
        cy.get('#create-input').type('Test Calculator 2')
        cy.get('.item-row > .fa-plus-circle').click()

        //Create first checkbox
        cy.get(':nth-child(1) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('First Checkbox')
        cy.get('.add-options > :nth-child(2) > input').type('1000')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()

        //Create second checkbox
        cy.get(':nth-child(1) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Second Checkbox')
        cy.get('.add-options > :nth-child(2) > input').type('2000')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()

        //Create third checkbox
        cy.get(':nth-child(1) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Third Checkbox')
        cy.get('.add-options > :nth-child(2) > input').type('777')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()

        //add total
        cy.get(':nth-child(8) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > input').clear()
        cy.get(':nth-child(1) > input').type('Total')
        cy.get(':nth-child(2) > textarea').type(' checkbox_field_id_0  +  checkbox_field_id_1  +  checkbox_field_id_2 ')
        cy.get('.green > span').click()

        //Open preview
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('[data-id="checkbox_field_id_0"] > .calc-checkbox > .calc-checkbox-item > label').click()
        cy.get('[data-id="checkbox_field_id_1"] > .calc-checkbox > .calc-checkbox-item > label').click()
        cy.get('[data-id="checkbox_field_id_2"] > .calc-checkbox > .calc-checkbox-item > label').click()
        cy.compareSnapshot('CCalc_3_check_boxes')



    })
})