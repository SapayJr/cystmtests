beforeEach(() => {
    cy.viewport(1920, 1080)
  })


describe('Demo Import',()=>{
    it('Demo Import and Check',()=>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#toplevel_page_cost_calculator_builder > .wp-has-submenu > .wp-menu-name').click()
        cy.get('.green > span').click()
        cy.get(':nth-child(1) > .ccb-demo-import-button').click()
        cy.wait(2000)
        cy.get('.existing-wrapper > :nth-child(2)').should('be.visible')
        cy.get('.existing-wrapper > :nth-child(3)').should('be.visible')
        cy.get('.existing-wrapper > :nth-child(4)').should('be.visible')
        cy.get('.existing-wrapper > :nth-child(5)').should('be.visible')
        cy.get('.existing-wrapper > :nth-child(6)').should('be.visible')


        //Delete imported calculators
        cy.get(':nth-child(2) > .actions > :nth-child(3) > .fas').click()
        cy.wait(2000)
        cy.get(':nth-child(2) > .actions > :nth-child(3) > .fas').click()
        cy.wait(2000)
        cy.get(':nth-child(2) > .actions > :nth-child(3) > .fas').click()
        cy.wait(2000)
        cy.get(':nth-child(2) > .actions > :nth-child(3) > .fas').click()
        cy.wait(2000)
        cy.get(':nth-child(2) > .actions > :nth-child(3) > .fas').click()
        cy.wait(2000)




    })
})