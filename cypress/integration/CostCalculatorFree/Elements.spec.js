beforeEach(() => {
    cy.viewport(1920, 1080)
  })

describe('Create and chek elements', ()=>{
    it('Create and check elements',()=>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#toplevel_page_cost_calculator_builder > .wp-has-submenu > .wp-menu-name').click()
        cy.wait(2000)
        cy.get('.button-wrapper > button:nth-child(2)').click()
        cy.get('#create-input').type('Test Elements')
        cy.get('.item-row > .fa-plus-circle').click()

        //Create Drop down element
        cy.get(':nth-child(5) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Price')
        cy.get('.drop-down-wrapper > :nth-child(1) > :nth-child(2) > input').clear()
        cy.get('.drop-down-wrapper > :nth-child(1) > :nth-child(2) > input').type('Services Price')
        cy.get('.add-options > :nth-child(1) > input').clear()
        cy.get('.add-options > :nth-child(1) > input').type('Price 1')
        cy.get('.add-options > :nth-child(2) > input').clear()
        cy.get('.add-options > :nth-child(2) > input').type('1000')
        cy.get(':nth-child(7) > .list-content--header > .green > span').click()
        cy.get(':nth-child(3) > :nth-child(1) > input').clear()
        cy.get(':nth-child(3) > :nth-child(1) > input').type('Price 2')
        cy.get(':nth-child(3) > :nth-child(2) > input').clear()
        cy.get(':nth-child(3) > :nth-child(2) > input').type('2000')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.calc-drop-down').select('Price 1')
        cy.get('.dropDown_field_id_0 > .sub-item-title').should('have.text',' Price')
        cy.get('.dropDown_field_id_0 > .sub-item-value').should('have.text',' 1000 ')
        cy.get('.calc-drop-down').select('Price 2')
        cy.get('.dropDown_field_id_0 > .sub-item-title').should('have.text',' Price')
        cy.get('.dropDown_field_id_0 > .sub-item-value').should('have.text',' 2000 ')
        cy.get('.ccb-close-btn-div').click()

        //Create HTML 
        cy.get(':nth-child(7) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > textarea').clear()
        cy.get(':nth-child(1) > textarea').type('<head><title>Calculator</title></head><body><h1>Calculator</h1><p>calculate for you</p></body>)')
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('h1').should('have.text','Calculator')
        cy.get('.html > p').should('have.text','calculate for you')
        cy.get('.html').should('be.visible')
        cy.get('.ccb-close-btn-div').click()


        //Create Line element
        cy.get(':nth-child(9) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > select').select('medium')
        cy.get(':nth-child(1) > select').select('large')
        cy.get(':nth-child(2) > select').select('dashed')
        cy.get(':nth-child(2) > select').select('solid')
        cy.get(':nth-child(3) > select').select('medium')
        cy.get(':nth-child(3) > select').select('long')
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.ccb-line').should('be.visible')
        cy.get('.ccb-close-btn-div').click()

        //Create Radio button
        cy.get(':nth-child(2) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Choose price')
        cy.get('.add-options > :nth-child(1) > input').clear()
        cy.get('.add-options > :nth-child(1) > input').type('price1')
        cy.get('.add-options > :nth-child(2) > input').clear()
        cy.get('.add-options > :nth-child(2) > input').type('100')
        cy.get(':nth-child(7) > .list-content--header > .green > span').click()
        cy.get(':nth-child(3) > :nth-child(1) > input').clear()
        cy.get(':nth-child(3) > :nth-child(1) > input').type('price2')
        cy.get(':nth-child(3) > :nth-child(2) > input').clear()
        cy.get(':nth-child(3) > :nth-child(2) > input').type('200')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.calc-radio > :nth-child(1) > label').click()
        cy.get('.radio_field_id_3 > .sub-item-title').should('have.text',' Choose price')
        cy.get('.radio_field_id_3 > .sub-item-value').should('have.text',' 100 ')
        cy.get(':nth-child(2) > label').click()
        cy.get('.radio_field_id_3 > .sub-item-title').should('have.text',' Choose price')
        cy.get('.radio_field_id_3 > .sub-item-value').should('have.text',' 200 ')
        cy.get('.ccb-close-btn-div').click()

        //Create Text box
        cy.get(':nth-child(6) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > input').clear()
        cy.get(':nth-child(1) > input').type('Your text')
        cy.get('.text-area-wrapper > :nth-child(1) > :nth-child(2) > input').clear()
        cy.get('.text-area-wrapper > :nth-child(1) > :nth-child(2) > input').type('Write here')
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('#text_area_').clear()
        cy.get('#text_area_').type('My Text')
        cy.get('.ccb-close-btn-div').click()

        //Create Range button
        cy.get(':nth-child(4) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Range')
        cy.get(':nth-child(5) > input').clear()
        cy.get(':nth-child(5) > input').type('5')
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.range_range_field_id_5').scrollIntoView().should('be.visible')
        cy.get('.ccb-close-btn-div').click()

        //Create Total field 
        cy.get(':nth-child(8) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(4) > .available-fields > :nth-child(1)').click()
        cy.get('[title="Subtraction (-)"]').click()
        cy.get(':nth-child(4) > .available-fields > :nth-child(2)').click()
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.calc-drop-down').select('Price 2')
        cy.get('.calc-radio > :nth-child(1) > label').click()
        cy.get('#total_field_id_6 > .sub-item-title').should('have.text',' Total description ')
        cy.get('#total_field_id_6 > .sub-item-value').should('have.text',' $ 1,900.00 ')
        cy.get('.ccb-close-btn-div').click()

        //Create Range slider
        cy.get(':nth-child(4) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Distance')
        cy.get(':nth-child(3) > input').clear()
        cy.get(':nth-child(3) > input').type('0')
        cy.get(':nth-child(4) > input').clear()
        cy.get(':nth-child(4) > input').type('1000')
        cy.get(':nth-child(5) > input').clear()
        cy.get(':nth-child(5) > input').type('1')
        cy.get(':nth-child(6) > input').clear()
        cy.get(':nth-child(6) > input').type('0')
        cy.get(':nth-child(7) > input').clear()
        cy.get(':nth-child(7) > input').type('Km')
        cy.get(':nth-child(8) > input').clear()
        cy.get(':nth-child(8) > input').type('2')
        cy.get('[style="margin-top: 38px;"] > .ccb-switch').click()
        cy.get('.green > span').click();
        cy.get('.calc-buttons > :nth-child(3)').click();
        cy.get('.range_range_field_id_7').scrollIntoView()
        cy.get('.range_range_field_id_7 > .e-slider-track').invoke('val','200').click()
        cy.get('.range_field_id_7 > .sub-item-title').should('have.text',' Distance')
        cy.get('.range_field_id_7 > .sub-item-value').should('have.text',' $ 998.00 ')
        cy.get('.ccb-close-btn-div').click()
        
        //Create quantity field
        let contentText
        cy.get(':nth-child(10) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Weight')
        cy.get('.quantity-wrapper > :nth-child(1) > :nth-child(2) > input').clear()
        cy.get('.quantity-wrapper > :nth-child(1) > :nth-child(2) > input').type('KG')
        cy.get(':nth-child(3) > input').clear()
        cy.get(':nth-child(3) > input').type('Enter your weight')
        cy.get(':nth-child(4) > input').clear()
        cy.get(':nth-child(4) > input').type('0')
        cy.get(':nth-child(5) > input').clear()
        cy.get(':nth-child(5) > input').type('1')
        cy.get('[style="margin-top: 38px;"] > .ccb-switch').click()
        cy.get('.green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.calc-input').scrollIntoView()
        for(let n = 0; n < 10; n ++){
        cy.get('.ccb-arrow-up').click()
        }    
        cy.get('.calc-input').clear()
        cy.get('.calc-input').type('10')
        cy.get('.ccb-close-btn-div').click()

        //Create toggle button
        cy.get(':nth-child(12) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').clear()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Delivery')
        cy.get('.toggle-wrapper > :nth-child(1) > :nth-child(2) > input').clear()
        cy.get('.toggle-wrapper > :nth-child(1) > :nth-child(2) > input').type('Delivery price')
        cy.get('[style="margin-top: 38px;"] > .ccb-switch').click()
        cy.get('.add-options > :nth-child(1) > input').clear()
        cy.get('.add-options > :nth-child(1) > input').type('With Delivery')
        cy.get('.add-options > :nth-child(2) > input').clear()
        cy.get('.add-options > :nth-child(2) > input').type('100')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.get('.calc-switch').scrollIntoView()
        cy.get('.calc-switch > label').click()
        cy.get('.sub-inner').scrollIntoView()
        cy.get('.sub-inner > .sub-item-title').should('have.text',' With Delivery ')
        cy.get('.sub-inner > .sub-item-value').should('have.text',' $ 100.00 ')
        cy.get('.ccb-close-btn-div').click()


        //add shortcut to the new page
        cy.get('.calc-buttons > :nth-child(1)').click()
        cy.wait(1000)
        cy.get('.short-code > p').should(($p) =>{
            contentText = Cypress.$($p).text();
            return contentText;
        })
        cy.get('[aria-haspopup="true"] > .ab-label').click()
        cy.get('.components-modal__frame > .components-modal__content > .components-modal__header > .components-button > svg').click()
        cy.get('#post-title-0').type('Calculator Elements')
        cy.get('.wp-block > .components-dropdown').click()
        cy.get('.block-editor-inserter__search-input').clear()
        cy.get('.block-editor-inserter__search-input').type('short')
        cy.get('.editor-block-list-item-shortcode svg').click()
        .then(()=>{
            cy.get('#blocks-shortcode-input-0').type(contentText)
        })
        cy.get('.editor-post-publish-panel__toggle').click()
        cy.get('.editor-post-publish-panel__header-publish-button > .components-button').click()
        // cy.get('.components-snackbar__content > .components-button').click()
        cy.get('.post-publish-panel__postpublish-buttons > a.components-button').click()
        cy.get('.calc-drop-down').select('Price 1')
        cy.get('.dropDown_field_id_0 > .sub-item-title').should('have.text',' Price')
        cy.get('.dropDown_field_id_0 > .sub-item-value').should('have.text',' 1000 ')
        cy.get(':nth-child(2) > label').click()
        cy.get('.radio_field_id_3 > .sub-item-value').should('have.text',' 200 ')
        cy.get('#total_field_id_6 > .sub-item-value').should('have.text',' $ 800.00 ')
        cy.get('.html > h1').should('have.text','Calculator')
        cy.get('.html > p').should('have.text','calculate for you')
        cy.get('.ccb-line').should('be.visible')
        cy.get('[data-id="radio_field_id_3"]').should('be.visible')
        cy.get('#text_area_').clear()
        cy.get('#text_area_').type('Test text')
        cy.get('.range_range_field_id_5 > .e-slider-track').invoke('val','10').click()
        cy.get('.range_range_field_id_7 > .e-slider-track').invoke('val','100').click()
        cy.get('.range_field_id_5 > .sub-item-value').should('have.text',' 50 ')
        cy.get('.range_field_id_7 > .sub-item-value').should('have.text',' $ 998.00 ') 
        cy.get('[data-id="quantity_field_id_8"]').should('be.visible')
        cy.get('.calc-input').scrollIntoView()
        for(let n = 0; n < 10; n ++){
            cy.get('.ccb-arrow-up').click()
            }; 
        cy.get('.quantity_field_id_8 > .sub-item-value').should('have.text',' $ 10.00 ')   
        cy.get('.calc-input').clear().type('100')
        cy.get('.quantity_field_id_8 > .sub-item-value').should('have.text',' $ 1,000.00 ')
        cy.get('.calc-switch > label').scrollIntoView()
        cy.get('.calc-switch > label').click()
        cy.get('.sub-inner > .sub-item-title').should('have.text',' With Delivery ')
        cy.get('.sub-inner > .sub-item-value').should('have.text',' $ 100.00 ')
        cy.wait(2000)
        cy.get('#wp-admin-bar-wp-logo > [aria-haspopup="true"]').click()
        cy.wait(1000)
        cy.get('#toplevel_page_cost_calculator_builder > .wp-has-submenu > .wp-menu-name').click()
        cy.get(':nth-child(2) > .actions > :nth-child(3)').click()






















        



    })
})