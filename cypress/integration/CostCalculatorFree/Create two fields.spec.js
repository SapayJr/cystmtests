describe('Create Calculator Fields', ()=>{

    beforeEach(() => {
        cy.viewport(1920, 1080)
      })

    it('Create two fields on the calculator form.', () =>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#toplevel_page_cost_calculator_builder > .wp-has-submenu > .wp-menu-name').click()
        cy.wait(2000)
        cy.get('.button-wrapper > button:nth-child(2)').click()
        cy.get('#create-input').type('Test Calculator')
        cy.get('.item-row > .fa-plus-circle').click()

        //create first field
        cy.get(':nth-child(4) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Range')
        cy.get('.range-wrapper > :nth-child(1) > :nth-child(2) > input').type('choose your destiny')
        cy.get('.range-wrapper > :nth-child(1) > :nth-child(2) > input').click()
        cy.get('.green > span').click()

        //create second field
        cy.get(':nth-child(1) > .calc-field__container > .fa-plus').click()
        cy.get(':nth-child(1) > :nth-child(1) > input').type('Click here pls')
        cy.get('[style="margin-top: 30px;"] > .list-content > :nth-child(1) > .green > span').click()

        //looking preview
        cy.get('.calc-buttons > :nth-child(3)').click()
        cy.compareSnapshot('CCalc_Two_Fields')


    })

})