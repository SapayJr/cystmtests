export default function HomePressRegressionTesting(url) {

    let uniqueLinks = [], domLinks = [], isSameURl = false;
    before(() => {
        cy.viewport(1920, 1080)
    })

    it('Visual Regression Testing', () => {

        cy.visit(url);
        cy.document().then((doc) => {
            var x = doc.querySelectorAll('a');
            for (var i=0; i<x.length; i++){
                if(
                    x[i].href.includes(x[i].baseURI) &&
                    !x[i].href.includes(x[i].baseURI+"#"
                    )){

                    console.log('site link = ',x[i].href)
                    let siteLink = x[i].href.toString();
                    if(siteLink.indexOf('/#')> -1 || siteLink.indexOf('/?')> -1 || siteLink.indexOf('/wp-content/')> -1){
                        console.log('reshetka = ',x[i].href)

                    }
                    else {
                        var nametext = x[i].textContent;
                        var cleantext = nametext.replace(/\s+/g, ' ').trim();
                        var cleanlink = x[i].href;
                        domLinks.push([cleantext,cleanlink]);
                    }
                }
            };
            for(var i=0; i<domLinks.length; i++){
                for(var j=0; j<uniqueLinks.length; j++){
                    if(domLinks[i][1] == uniqueLinks[j][1]){
                        isSameURl = true;
                    }
                }
                if(!isSameURl){
                    uniqueLinks.push(domLinks[i][1]);
                }
                else {
                    isSameURl = false;
                }
            }

            var arrayWithoutDuplicates = [];
            for(i=0; i < uniqueLinks.length; i++){
                if(arrayWithoutDuplicates.indexOf(uniqueLinks[i]) === -1) {
                    arrayWithoutDuplicates.push(uniqueLinks[i]);
                }
            }


            cy.log('Links: ', arrayWithoutDuplicates.length)
            cy.log(arrayWithoutDuplicates)
            for(var i=0; i<arrayWithoutDuplicates.length; i++){

                let siteLink = arrayWithoutDuplicates[i];

                cy.log('left: ', (arrayWithoutDuplicates.length-i))
                cy.task('log', '  Visiting  - ' + arrayWithoutDuplicates[i])
                cy.task('log', '  Capture  - 1920x1080')
                cy.viewport(1920, 1080)
                cy.visit({url:siteLink, failOnStatusCode: false});

                cy.task('log', '  Visited  - ' + arrayWithoutDuplicates[i])
                let siteUrlForLog = arrayWithoutDuplicates[i];


                cy.document().then((dom) => {

                    dom.body.className = dom.body.className.replace("sticky_menu","");
                    dom.body.className = dom.body.className.replace("affix","");
                    const removeElements = (elms) => elms.forEach(el => el.remove());
                    removeElements( dom.querySelectorAll(".pearl-envato-preview") );
                    removeElements( dom.querySelectorAll(".pearl-envato-preview-holder") );

                    // dom.getElementsByClassName('header_top clearfix affix').setAttribute("style", "top:0px !important");
                    var isFullHeight = dom.getElementsByClassName('inventory-full-height');
                    if(dom.documentElement.offsetHeight > dom.documentElement.scrollHeight  && isFullHeight.length == 0){
                        cy.task('log', '  Scrolling  - ' + siteUrlForLog)
                        cy.scrollTo('0%', '25%',{ duration: 2000 },{ ensureScrollable: false })
                        cy.scrollTo('0%', '50%',{ duration: 2000 },{ ensureScrollable: false })
                        cy.scrollTo('0%', '75%',{ duration: 2000 },{ ensureScrollable: false })
                        cy.scrollTo('0%', '100%',{ duration: 2000 },{ ensureScrollable: false })
                        cy.wait(2000)
                        cy.task('log', '  Scrolling Ended  - ' + siteUrlForLog)

                    }
                });

                var screenshotName = arrayWithoutDuplicates[i].replace('https://','').replaceAll('/','__');
                screenshotName = screenshotName.replaceAll('.','__').toString();

                cy.task('log', '  Executing a script for a screenshot  - ' + siteUrlForLog)

                cy.compareSnapshot(screenshotName,{capture:'fullPage',width: 1920, height: 1080});

                cy.task('log', '  Page Index  - ' + i)
                cy.task('log', '  Screenshot successfully taken from this page - ' + arrayWithoutDuplicates[i])
                cy.task('log', '  Number of pages remaining: ' + (arrayWithoutDuplicates.length-i))
                cy.task('log', '   ' )

            }

        })

    })
}



