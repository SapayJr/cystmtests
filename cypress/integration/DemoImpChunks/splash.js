export default function SplashRegressionTesting(url,demo) {


    let demos = [



        'https://splash.stylemixthemes.com/basketball/',
        'https://splash.stylemixthemes.com/soccer/',
        'https://splash.stylemixthemes.com/football/',
        'https://splash.stylemixthemes.com/rugby/',
        'https://splash.stylemixthemes.com/volleyball/',
        'https://splash.stylemixthemes.com/esport/',
        'https://splash.stylemixthemes.com/basketball-two/',
        'https://splash.stylemixthemes.com/soccer-two/',
        'https://splash.stylemixthemes.com/hockey/',
        'https://splash.stylemixthemes.com/baseball/',
        'https://splash.stylemixthemes.com/magazine-one/',
        'https://splash.stylemixthemes.com/magazine-two/',
        'https://splash.stylemixthemes.com/soccer-news/'

    ]
    let uniqueLinks = [], domLinks = [], isSameURl = false;
    beforeEach(() => {
        cy.viewport(1920, 1080)
      })
      Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })

    it('Visual Regression Testing', () => {

        cy.visit(url);
        cy.document().then((doc) => {
            var x = doc.querySelectorAll('a');
                for (var i=0; i<x.length; i++){
                    if( x[i].href.includes(x[i].baseURI) &&
                         !x[i].href.includes(x[i].baseURI+"#" &&
                            !x[i].href.includes(x[i].baseURI+"landing/")
                         )) {

                        console.log('site link = ',x[i].href)
                        let siteLink = x[i].href.toString();
                        if(siteLink.indexOf('/#')> -1 || siteLink.indexOf('/?')> -1 || siteLink.indexOf('/landing')> -1){
                            console.log('reshetka = ',x[i].href)
                        }
                        else {
                            var nametext = x[i].textContent;
                            var cleantext = nametext.replace(/\s+/g, ' ').trim();
                            var cleanlink = x[i].href;
                            domLinks.push([cleantext,cleanlink]);
                        }
                    }
                };
            for(var i=0; i<domLinks.length; i++){
                for(var j=0; j<uniqueLinks.length; j++){
                    if(domLinks[i][1] == uniqueLinks[j][1]){
                        isSameURl = true;
                    }
                }
                if(!isSameURl){
                    uniqueLinks.push(domLinks[i][1]);
                }
                else {
                    isSameURl = false;
                }
            }

            var arrayWithoutDuplicates = [];
            for(i=0; i < uniqueLinks.length; i++){
                if(arrayWithoutDuplicates.indexOf(uniqueLinks[i]) === -1) {
                    arrayWithoutDuplicates.push(uniqueLinks[i]);
                }
            }

            if(demo === 'NewYork','Dublin','Football') {
                arrayWithoutDuplicates = arrayWithoutDuplicates.filter(function(x) {
                    return demos.indexOf(x) < 0;
                });
            }

            cy.log('Links: ', arrayWithoutDuplicates.length)

            for(var i=0; i<arrayWithoutDuplicates.length; i++) {

                let siteLink = arrayWithoutDuplicates[i];

                cy.log('left: ', (arrayWithoutDuplicates.length-i))
                cy.task('log', '  Visiting  - ' + arrayWithoutDuplicates[i])

                cy.task('log', '  Visited  - ' + arrayWithoutDuplicates[i])
                cy.wait(4000);

                let siteUrlForLog = arrayWithoutDuplicates[i];
                cy.visit({url:siteLink, failOnStatusCode: false});

                cy.document().then((dom) => {

                    dom.body.className = dom.body.className.replace("sticky_menu","");
                    dom.body.className = dom.body.className.replace("affix","");

                    const removeElements = (elms) => elms.forEach(el => el.remove());
                    removeElements( dom.querySelectorAll(".pearl-envato-preview") );
                    removeElements( dom.querySelectorAll(".pearl-envato-preview-holder") );
                    removeElements( dom.querySelectorAll(".stm_theme_demos") );

                    // dom.getElementsByClassName('header_top clearfix affix').setAttribute("style", "top:0px !important");
                    if(dom.documentElement.offsetHeight > dom.documentElement.scrollHeight ){
                        cy.task('log', '  Scrolling  - ' + siteUrlForLog)

                        cy.scrollTo('0%', '25%',{ duration: 2000 })
                        cy.scrollTo('0%', '50%',{ duration: 2000 })
                        cy.scrollTo('0%', '75%',{ duration: 2000 })
                        cy.scrollTo('0%', '100%',{ duration: 2000 })

                        cy.wait(2000)
                        cy.task('log', '  Scrolling Ended  - ' + siteUrlForLog)

                    }
                });

                var screenshotName = arrayWithoutDuplicates[i].replace('https://','').replaceAll('/','__');
                screenshotName = screenshotName.replaceAll('.','__').toString();
                screenshotName = screenshotName.replaceAll('?','__')

                cy.wait(4000);
                cy.task('log', '  Executing a script for a screenshot  - ' + siteUrlForLog)
                cy.get('#frontend_customizer_button').hideElement()
                cy.get('#stm-footer-bottom').hideElement()
                cy.compareSnapshot(screenshotName, 0.2);

                cy.task('log', '  Page Index  - ' + i)
                cy.task('log', '  Screenshot successfully taken from this page - ' + arrayWithoutDuplicates[i])
                cy.task('log', '  Number of pages remaining: ' + (arrayWithoutDuplicates.length-i))
                cy.task('log', '   ')

            }

        })

    })
}

