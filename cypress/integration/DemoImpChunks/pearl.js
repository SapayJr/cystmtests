export default function PearlRegressionTesting(url) {

    let uniqueLinks = [], domLinks = [], isSameURl = false;
    before(() => {
        cy.viewport(1920, 1080)
    })
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })

    it('Visual Regression Testing', () => {

        cy.visit(url);
        cy.document().then((doc) => {
            var x = doc.querySelectorAll('a');
            for (var i=0; i<x.length; i++){
                if(
                    x[i].href.includes(x[i].baseURI) &&
                    !x[i].href.includes(x[i].baseURI+"#" &&
                        !x[i].href.includes(x[i].baseURI+"landing/"
                        ))){

                    console.log('site link = ',x[i].href)
                    let siteLink = x[i].href.toString();
                    if(siteLink.indexOf('/#')> -1 || siteLink.indexOf('/?')> -1 || siteLink.indexOf('/landing')> -1){
                        console.log('reshetka = ',x[i].href)

                    }
                    else {
                        var nametext = x[i].textContent;
                        var cleantext = nametext.replace(/\s+/g, ' ').trim();
                        var cleanlink = x[i].href;
                        domLinks.push([cleantext,cleanlink]);
                    }
                }
            };
            for(var i=0; i<domLinks.length; i++){
                for(var j=0; j<uniqueLinks.length; j++){
                    if(domLinks[i][1] == uniqueLinks[j][1]){
                        isSameURl = true;
                    }
                }
                if(!isSameURl){
                    uniqueLinks.push(domLinks[i][1]);
                }
                else {
                    isSameURl = false;
                }
            }

            var arrayWithoutDuplicates = [];
            for(i=0; i < uniqueLinks.length; i++){
                if(arrayWithoutDuplicates.indexOf(uniqueLinks[i]) === -1) {
                    arrayWithoutDuplicates.push(uniqueLinks[i]);
                }
            }

            cy.log('Links: ', arrayWithoutDuplicates.length)


            for(var i=0; i<arrayWithoutDuplicates.length; i++){
                cy.log('left: ', (arrayWithoutDuplicates.length-i))
                cy.visit(arrayWithoutDuplicates[i]);
                cy.get('body').its('.pearl-envato-preview').then(res=>{

                    if(res > 0)
                    {
                        cy.get('.pearl-envato-preview').invoke('attr', 'style', 'display: none');
                        cy.get('.pearl-envato-preview-holder').invoke('attr', 'style', 'display: none');
                        cy.get('.preview__envato-logo').invoke('attr', 'style', 'display: none');
                        cy.get('.preview__actions').invoke('attr', 'style', 'display: none');
                    }
                    else
                    {
                        console.log('iframe false')
                    }

                });

                cy.document().then((dom) => {
                    dom.body.className = dom.body.className.replace("sticky_menu","");
                    dom.body.className = dom.body.className.replace("affix","");
                    const removeElements = (elms) => elms.forEach(el => el.remove());
                    removeElements( dom.querySelectorAll(".pearl-envato-preview") );
                    removeElements( dom.querySelectorAll(".pearl-envato-preview-holder") );

                    // dom.getElementsByClassName('header_top clearfix affix').setAttribute("style", "top:0px !important");
                    if(dom.documentElement.scrollHeight>1080){
                        cy.scrollTo('0%', '25%',{ duration: 2000 })
                        cy.scrollTo('0%', '50%',{ duration: 2000 })
                        cy.scrollTo('0%', '75%',{ duration: 2000 })
                        cy.scrollTo('0%', '100%',{ duration: 2000 })
                        cy.wait(2000)
                    }
                });

                var screenshotName = arrayWithoutDuplicates[i].replace('https://','').replaceAll('/','__');
                screenshotName = screenshotName.replaceAll('.','__').toString();

                cy.wait(4000);

                cy.compareSnapshot(screenshotName, 0.2);

            }

        })

    })
}



