beforeEach(() => {
    cy.viewport(1920, 1080)
  })
  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })
  



describe('Milan Demo Import',()=>{

    const getIframeDocument = () => {
        return cy
        .get('#elementor-preview-iframe')
        // Cypress yields jQuery element, which has the real
        // DOM element under property "0".
        // From the real DOM iframe element we can get
        // the "document" element, it is stored in "contentDocument" property
        // Cypress "its" command can access deep properties using dot notation
        // https://on.cypress.io/its
        .its('0.contentDocument').should('exist')
      }
      
      const getIframeBody = () => {
        // get the document
        return getIframeDocument()
        // automatically retries until body is loaded
        .its('body').should('not.be.undefined')
        // wraps "body" DOM element to allow
        // chaining more Cypress commands, like ".find(...)"
        .then(cy.wrap)
      }
    it('Should import Milan demo',()=>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        // cy.get('#toplevel_page_stm-admin > .wp-has-submenu > .wp-menu-name').click()
        // cy.wait(1000)
        // cy.get('[href="http://demo-import.stylemixstage.com/wp-admin/admin.php?page=stm-admin-demos"]').click()
        // cy.get('.top-panel > .float_left > :nth-child(3)').click()
        // cy.get(':nth-child(59) > .inner').trigger('mouseover')
        // cy.get(':nth-child(59) > .inner').contains('Import Demo').click({force: true})
        // cy.get('.stm_install__demo_start').click()
        // cy.wait(250000)
        // cy.reload()
        // cy.get('.zoom_close').click()
        cy.get('#menu-pages > .wp-has-submenu > .wp-menu-name').click()
        cy.get('#menu-pages > .wp-submenu > :nth-child(3) > a').click()
        cy.get('.components-modal__frame > .components-modal__content > .components-modal__header > .components-button > svg').click()
        cy.get('.wpb_switch-to-composer').click()
        cy.wait(20000)
            getIframeBody().find('.eicon-folder').should('be.visible').click()
            cy.get('.elementor-template-library-template:nth-child(8) .elementor-template-library-template-preview').click()      
            cy.get('.elementor-template-library-template-action').click()
            cy.get('#elementor-panel-saver-button-publish-label').click()   
        // cy.get('.eicon-folder').click({force : true})


    })
})