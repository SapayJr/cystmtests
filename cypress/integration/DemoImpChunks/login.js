export default function login(login, password) {
    it('WP admin visit', () => {
        cy.visit('https://testing.uz/wp-admin') // visiting the site
        cy.get('#user_login').type(login)
        cy.get('#user_pass').type(password)
        cy.get('.forgetmenot #rememberme').click()
        cy.get('#wp-submit').contains('Log In').click()
    })
  }