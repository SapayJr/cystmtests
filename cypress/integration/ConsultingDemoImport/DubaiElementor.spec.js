beforeEach(() => {
    cy.viewport(1920, 1080)
  })
  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })
  



describe('Dubai Demo Import',()=>{
    it('Should import Dubai demo',()=>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#menu-appearance > .wp-has-submenu > .wp-menu-name').click()
        cy.get('[aria-describedby="consulting-action consulting-name"] > .theme-id-container > .theme-actions > .activate').contains('Activate').click({force : true})
        cy.wait(1000)
        cy.get('[href="http://demo-import.stylemixstage.com/wp-admin/admin.php?page=stm-admin-demos"]').click()
        cy.get('.top-panel > .float_left > :nth-child(3)').click()
        cy.get(':nth-child(83) > .inner').trigger('mouseover')
        cy.get(':nth-child(83) > .inner').contains('Import Demo').click({force: true})
        cy.get('.stm_install__demo_start').click()
        cy.wait(250000)
        cy.reload()
        cy.get('.zoom_close').click()
        cy.get('#menu-settings > .wp-has-submenu > .wp-menu-name').click()
        cy.get('#menu-settings > .wp-submenu > :nth-child(7) > a').click()
        cy.get(':nth-child(5) > th > label').click()
        cy.get('#submit').click()
})
    it('HomePage',()=>{
        cy.visit('http://demo-import.stylemixstage.com/')
        cy.wait(2000)
        cy.compareSnapshot('HomePage',0.2)
    })
    it('AboutLayout1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/')
        cy.wait(2000)
        cy.compareSnapshot('AboutLayout1',0.2)
    })
    it('AboutLayout2',()=>{
        cy.visit('http://demo-import.stylemixstage.com/about-layout-2/')
        cy.wait(2000)
        cy.compareSnapshot('AboutLayout2',0.2)
    })
    it('ABoutLayout3',()=>{
        cy.visit('http://demo-import.stylemixstage.com/about-layout-3/')
        cy.wait(2000)
        cy.compareSnapshot('AboutLayout3',0.2)
    })
    it('OurApproach',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/our-approach/')
        cy.wait(2000)
        cy.compareSnapshot('OurApproach',0.2)
    })
    it('Partners',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/our-partners/')
        cy.wait(2000)
        cy.compareSnapshot('Partners',0.2)
    })
    it('JobsListing',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/careers/')
        cy.wait(2000)
        cy.compareSnapshot('JobsListing',0.2)
    })
    it('VacancyPage1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/careers_archive/deputy-principal-construction-manager-2/')
        cy.wait(2000)
        cy.compareSnapshot('VacancyPage1',0.2)
    })
    it('VacancyPage2',()=>{
        cy.visit('http://demo-import.stylemixstage.com/careers_archive/senior-industrial-planner/')
        cy.wait(2000)
        cy.compareSnapshot('Vacancypage2',0.2)
    })
    it('TeamList',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/our-team-list/')
        cy.wait(2000)
        cy.compareSnapshot('TeamList',0.2)
    })
    it('TeamGrid',()=>{
        cy.visit('http://demo-import.stylemixstage.com/company-overview/our-team-grid/')
        cy.wait(2000)
        cy.compareSnapshot('TeamGrid',0.2)
    })
    it('TeamMember',()=>{
        cy.visit('http://demo-import.stylemixstage.com/staff/brandon-copperfield/')
        cy.wait(2000)
        cy.compareSnapshot('TeamMember',0.2)
    })
    it('ServicesGrid1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/services-grid/')
        cy.wait(2000)
        cy.compareSnapshot('ServicesGrid1',0.2)
    })
    it('CasesGrid1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/our-work-grid/')
        cy.wait(2000)
        cy.compareSnapshot('CasesGrid1',0.2)
    })
    it('CasesWithFilter',()=>{
        cy.visit('http://demo-import.stylemixstage.com/our-work-with-filter/')
        cy.wait(2000)
        cy.compareSnapshot('CasesWithFilter',0.2)
    })
    it('SingleCaseLayout1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/works/applying-commercial-excellence-in-chemicals/')
        cy.wait(2000)
        cy.compareSnapshot('SingleCaseLayout1',0.2)
    })
    it('SingleCaselayout2',()=>{
        cy.visit('http://demo-import.stylemixstage.com/works/constructing-a-best-in-class-global/')
        cy.wait(2000)
        cy.compareSnapshot('SingleCaseLayout2',0.2)
    })
    it('SingleCaseLayout3',()=>{
        cy.visit('http://demo-import.stylemixstage.com/works/transformation-sparks-financial-leaders-turnaround/')
        cy.wait(2000)
        cy.compareSnapshot('SingleCaseLayout3',0.2)
    })
    it('SingleCaselayout4',()=>{
        cy.visit('http://demo-import.stylemixstage.com/works/focus-on-core-delivers-growth-for-retailer/')
        cy.wait(2000)
        cy.compareSnapshot('SingleCaseLayout4',0.2)
    })
    it('SingleCaselayout5',()=>{
        cy.visit('http://demo-import.stylemixstage.com/works/healthcare-giant-overcomes-merger-risks-for-growth/')
        cy.wait(2000)
        cy.compareSnapshot('SingleCaseLayout5',0.2)
    })
    it('NewsListView',()=>{
        cy.visit('http://demo-import.stylemixstage.com/blog/')
        cy.wait(2000)
        cy.compareSnapshot('NewsListView',0.2)
    })
    it('NewsGridView',()=>{
        cy.visit('http://demo-import.stylemixstage.com/blog/?layout=grid&sidebar_id=none')
        cy.wait(2000)
        cy.compareSnapshot('NewsGridView',0.2)
    })
    it('PortfolioSingleOne',()=>{
        cy.visit('http://demo-import.stylemixstage.com/portfolio/beff-baffer-construction/')
        cy.wait(2000)
        cy.compareSnapshot('PortfolioSingleOne',0.2)
    })
    it('PortfolioSingleTwo',()=>{
        cy.visit('http://demo-import.stylemixstage.com/portfolio/focus-on-core-delivers-growth-for-retailer/')
        cy.wait(2000)
        cy.compareSnapshot('PortfolioSingleTwo',0.2)
    })
    it('PortfolioSingleThree',()=>{
        cy.visit('http://demo-import.stylemixstage.com/portfolio/construction-of-railways/')
        cy.wait(2000)
        cy.compareSnapshot('PortfolioSingleThree',0.2)
    })
    it('Testimonials1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/testimonials-page/')
        cy.wait(2000)
        cy.compareSnapshot('Testimonials1',0.2)
    })
    it('Testimonials2',()=>{
        cy.visit('http://demo-import.stylemixstage.com/layout-2/')
        cy.wait(2000)
        cy.compareSnapshot('Testimonials2',0.2)
    })
    it('Typography',()=>{
        cy.visit('http://demo-import.stylemixstage.com/typography/')
        cy.wait(2000)
        cy.compareSnapshot('Typography',0.2)
    })
    it('EventsGrid',()=>{
        cy.visit('http://demo-import.stylemixstage.com/events-grid/')
        cy.wait(2000)
        cy.compareSnapshot('EventsGrid',0.2)
    })
    it('EventClassic',()=>{
        cy.visit('http://demo-import.stylemixstage.com/events-classic/')
        cy.wait(2000)
        cy.compareSnapshot('EventClassic',0.2)
    })
    it('EventModern',()=>{
        cy.visit('http://demo-import.stylemixstage.com/events-modern/')
        cy.wait(2000)
        cy.compareSnapshot('EventModern',0.2)
    })
    it('Appointment',()=>{
        cy.visit('http://demo-import.stylemixstage.com/make-an-appointment/')
        cy.wait(2000)
        cy.compareSnapshot('Appointment',0.2)
    })
    it('MyAccounts',()=>{
        cy.visit('http://demo-import.stylemixstage.com/shop-2/my-account/')
        cy.wait(2000)
        cy.compareSnapshot('MyAccounts',0.2)
    })
    it('Cart',()=>{
        cy.visit('http://demo-import.stylemixstage.com/shop-2/cart/')
        cy.wait(2000)
        cy.compareSnapshot('Cart',0.2)
    })
    it('ContactUs1',()=>{
        cy.visit('http://demo-import.stylemixstage.com/contact-us/')
        cy.wait(2000)
        cy.compareSnapshot('ContactUs1',0.2)
    })
    it('ContactUs2',()=>{
        cy.visit('http://demo-import.stylemixstage.com/contact-two/')
        cy.wait(2000)
        cy.compareSnapshot('ContactUs2',0.2)
    })
    it('ContactUs3',()=>{
        cy.visit('http://demo-import.stylemixstage.com/contact-three/')
        cy.wait(2000)
        cy.compareSnapshot('ContactUs3',0.2)
    })
    it('ContactUs4',()=>{
        cy.visit('http://demo-import.stylemixstage.com/contact-four/')
        cy.wait(2000)
        cy.compareSnapshot('ContactUs4',0.2)
    })
    it('ContactUs5',()=>{
        cy.visit('http://demo-import.stylemixstage.com/contact-five/')
        cy.wait(2000)
        cy.compareSnapshot('ContactUs5',0.2)
    })
    it('Reset Wordpress',()=>{
        cy.visit('http://demo-import.stylemixstage.com/wp-login.php')
        cy.wait(2000)
        cy.get('#user_login').type('Test')
        cy.get('#user_pass').type('gJ)LiB3E!lwHru&J)w')
        cy.get('#wp-submit').click()
        cy.get('#menu-tools > .wp-has-submenu > .wp-menu-name').click()
        cy.get('#menu-tools > .wp-submenu > :nth-child(8) > a').click()
        cy.get('#wp_reset_confirm').type('reset')
        cy.get('#wp_reset_submit').click()
        cy.wait(1000)
        cy.get('.swal2-confirm').click()

})


})